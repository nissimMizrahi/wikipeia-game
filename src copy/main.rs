extern crate reqwest;
extern crate serde_json;

use std::sync::{Mutex, Arc};
use std::thread;

#[macro_use]
mod Wikipedia;
use Wikipedia::WikiClient;

mod DiskBufferMod;
use DiskBufferMod::DiskBuffer;

const g_nonWords: &[&str] = &["of", "the", "in", "a"];

fn getDescribingWords(client: &WikiClient, page: &str, nonWords: &[&str]) -> Vec<String> {
    let mut ret = Vec::new();

    if let Ok(categories) = client.getParseList(page, "categories") {
        for category in categories
            .into_iter()
            .map(|l| l.to_lowercase())
            .filter(|l| {
                !(l.contains("wikipedia")
                    || l.contains("articles")
                    || l.contains("cs1")
                    || l.contains("pages"))
                //true
            })
        {
            let mut words = category
                .split(&['_', '-'][..])
                .filter(|word| !nonWords.contains(word))
                .map(|word| String::from(word))
                .collect::<Vec<String>>();
            ret.append(&mut words);
        }
    }
    ret.sort();
    ret.dedup();
    return ret;
}

fn getCorelation(client: Mutex<WikiClient>, page1: &str, page2: &str, nonWords: &[&str]) -> f32 {
    let mut a = Vec::new();
    let mut b = Vec::new();

    {
        let cl = client.lock().unwrap();
        a = getDescribingWords(&cl, page1, nonWords);
        b = getDescribingWords(&cl, page2, nonWords);
    }

    let mut c = a.clone();

    c.append(&mut b);
    c.sort();
    let l = c.len();
    c.dedup_by(|x, y| return x.contains(&y[..]) || x.contains(&y[..]));

    return (l - c.len()) as f32 / a.len().min(b.len()) as f32;
}

fn main() {
    let wiki = WikiClient::new();

    let mut results = Vec::new();
    let mut threads = Vec::new();

    let src = "israel";

    let res = wiki.getParseList(src, "links");

    let mutex = Arc::new(Mutex::new(wiki));

    if let Ok(links) = res {
        for link in links {
            let copy = Arc::clone(&mutex);
            threads.push(thread::spawn(move || {

                let mut a = Vec::new();
                let mut b = Vec::new();

                {
                    let cl = copy.lock().unwrap();
                    a = getDescribingWords(&cl, &link, g_nonWords);
                    b = getDescribingWords(&cl, src, g_nonWords);
                }

                let mut c = a.clone();

                c.append(&mut b);
                c.sort();
                let l = c.len();
                c.dedup_by(|x, y| return x.contains(&y[..]) || x.contains(&y[..]));

                return (link, (l - c.len()) as f32 / a.len().min(b.len()) as f32);
            }));
        }
        //println!("{:#?}", results);
    }

    for t in threads {
        results.push(t.join().unwrap());
    }
    results = results
        .into_iter()
        .filter(|(link, score)| !score.is_nan())
        .collect::<Vec<(String, f32)>>();
    results.sort_by(|(_, score1), (_, score2)| score1.partial_cmp(score2).unwrap());
}
