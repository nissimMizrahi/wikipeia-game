extern crate reqwest;
extern crate serde_json;

macro_rules! WIKI {
    ($page:expr, links) => {
    [
        ("action", "parse"),
        ("prop", "links"),
        ("page", $page)
    ]
    };
    ($page:expr, categories) => {
    [
        ("action", "parse"),
        ("prop", "categories"),
        ("page", $page)
    ]
    };

    (categories_arr) => {"categories"};
    (links_arr) => {"links"};
}


pub struct WikiClient {
    client: reqwest::r#async::Client,
}


impl WikiClient {
    pub fn new() -> WikiClient {
        WikiClient {
            client: reqwest::r#async::Client::new(),
        }
    }
    pub fn get(&self, params: &[(&str, &str)]) -> Result<(serde_json::Value, usize), String> {

        let mut tmp = params.to_vec();
        tmp.push(("format", "json"));

        match self.client.get("https://en.wikipedia.org/w/api.php").query(&tmp).send().map(|ret| {
            if let Ok(text) = ret.text() {
                if let Ok(json) = serde_json::from_str(&text) {
                    return Ok((json, text.len()));
                }
            }
            Err(format!("barams, {:?}", ret))
        });
    }

    pub fn getParseList(&self, page: &str, arrName: &str) -> Result<Vec<String>, String>
    {
        match self.get(&[
            ("action", "parse"),
            ("prop", arrName),
            ("page", page)
        ]) {
            Ok((json, length)) =>
            {
                let mut ret = Vec::new();
                if let Some(arr) = json["parse"][arrName].as_array()
                {
                    for val in arr
                    {
                        if let Some(link) = val["*"].as_str()
                        {
                            ret.push(link.to_owned());
                        }

                    }
                    return Ok(ret);
                }
                return Err(String::from("cant parse list"));

            },

            Err(e) => {println!("{}", e); return Err(e)},
        }
    }
}
