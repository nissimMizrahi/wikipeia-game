extern crate rayon;
extern crate reqwest;
extern crate serde_json;

use rayon::prelude::*;
use rayon::ThreadPool;

use std::sync::{Arc, Mutex};
use std::thread;

use std::collections::BTreeMap;

macro_rules! WIKI {
    ($page:expr, links) => {
        [("action", "parse"), ("prop", "links"), ("page", $page)]
    };
    ($page:expr, categories) => {
        [("action", "parse"), ("prop", "categories"), ("page", $page)]
    };

    (categories_arr) => {
        "categories"
    };
    (links_arr) => {
        "links"
    };
}

pub struct WikiClient {
    client: reqwest::Client,
}

pub struct WikiSearcher {
    client: Arc<WikiClient>,
    lowest_depth: Arc<Mutex<u32>>,
    visited: Arc<Mutex<BTreeMap<String, u32>>>,

    end_categories: Arc<Mutex<Option<Vec<String>>>>,
}

impl WikiClient {
    pub fn new() -> WikiClient {
        WikiClient {
            client: reqwest::Client::new(),
        }
    }
    pub fn get(&self, params: &[(&str, &str)]) -> Result<(serde_json::Value, usize), String> {
        let mut tmp = params.to_vec();
        tmp.push(("format", "json"));

        match self
            .client
            .get("https://en.wikipedia.org/w/api.php")
            .query(&tmp)
            .send()
        {
            Ok(ref mut ret) => {
                if let Ok(text) = ret.text() {
                    if let Ok(json) = serde_json::from_str(&text) {
                        return Ok((json, text.len()));
                    }
                }
                Err(format!("barams, {:?}", ret))
            }
            Err(e) => Err(e.to_string()),
        }
    }

    pub fn getValidLinks(&self, page: &str) -> Result<Vec<String>, String> {
        match self.get(&[("action", "parse"), ("prop", "links"), ("page", &page)]) {
            Ok((json, length)) => {
                let mut ret = Vec::new();
                if let Some(arr) = json["parse"]["links"].as_array() {
                    for val in arr.into_iter().filter(|&val| match val.get("exists") {
                        Some(_) => true,
                        None => false,
                    }) {
                        if let Some(link) = val["*"].as_str() {
                            ret.push(link.to_owned());
                        }
                    }
                    return Ok(ret);
                }
                println!("err {}", page);
                return Err(String::from("cant parse list"));
            }

            Err(e) => return Err(e),
        }
    }

    pub fn getParseList(&self, page: &str, arrName: &str) -> Result<Vec<String>, String> {
        match self.get(&[
            ("action", "parse"),
            ("prop", arrName),
            ("page", &page.replace(" ", "_")),
        ]) {
            Ok((json, length)) => {
                let mut ret = Vec::new();
                if let Some(arr) = json["parse"][arrName].as_array() {
                    for val in arr {
                        if let Some(link) = val["*"].as_str() {
                            ret.push(link.to_owned());
                        }
                    }
                    return Ok(ret);
                }
                println!("err {}", page);
                return Err(String::from("cant parse list"));
            }

            Err(e) => return Err(e),
        }
    }
}

const g_nonWords: &[&str] = &["of", "the", "in", "a"];

impl WikiSearcher {
    pub fn new() -> WikiSearcher {
        WikiSearcher {
            client: Arc::new(WikiClient::new()),

            lowest_depth: Arc::new(Mutex::new(0)),
            visited: Arc::new(Mutex::new(BTreeMap::new())),

            end_categories: Arc::new(Mutex::new(None)),
        }
    }

    pub fn getDescribingWords(&self, page: &str, nonWords: &[&str]) -> Vec<String> {
        let mut ret = Vec::new();

        loop {
            match self.client.getParseList(page, "categories") {
                Ok(categories) => {
                    for category in categories
                        .into_iter()
                        .map(|l| l.to_lowercase())
                        .filter(|l| {
                            !(l.contains("wikipedia")
                                || l.contains("articles")
                                || l.contains("cs1")
                                || l.contains("pages"))
                            //true
                        })
                    {
                        let mut words = category
                            .split(&['_', '-'][..])
                            .filter(|word| !nonWords.contains(word))
                            .map(|word| String::from(word))
                            .collect::<Vec<String>>();
                        ret.append(&mut words);
                    }
                    break;
                }
                Err(e) => {
                    println!("error {} => {}", e, page);
                    //client = WikiClient::new();
                }
            }
        }
        ret.sort();
        ret.dedup();
        return ret;
    }

    pub fn getCorelation(&self, page1: &str, nonWords: &[&str]) -> f32 {
        let mut a = self.getDescribingWords(page1, nonWords);
        let mut end_cat = self.end_categories.lock().unwrap();
        if let Some(ref mut b) = *end_cat {
            let mut c = a.clone();

            c.extend_from_slice(&b[..]);
            c.sort();
            let l = c.len();
            c.dedup_by(|x, y| return x.contains(&y[..]) || x.contains(&y[..]));

            return if a.len() == 0 {
                0.0
            } else {
                (l - c.len()) as f32 / a.len().min(b.len()) as f32
            };
        } else {
            return 0.0;
        }
    }

    pub fn getPath(
        &'static self,
        start: &str,
        end: &'static str,
        depth: u32,
    ) -> (Vec<Vec<String>>, u32) {
        let mut results = Vec::new();
        let mut f_end_depth = 0;

        //{
        //    let mut end_cat = self.end_categories.lock().unwrap();
        //    if *end_cat == None {
        //        *end_cat = Some(self.getDescribingWords(end, g_nonWords));
        //    }
        //}
        //
        //println!(
        //    "{}'s score = {}",
        //    start,
        //    self.getCorelation(start, g_nonWords)
        //);

        if start == end {
            let mut ret = Vec::new();
            ret.push(Vec::new());
            ret[0].push(String::from(start));

            let mut lowest_depth = self.lowest_depth.lock().unwrap();
                //println!("end depth {}, my depth {}", end_depth, depth);
                if depth <= *lowest_depth || *lowest_depth == 0  {
                    println!("found {}", start);
                    (*lowest_depth) = depth;
                } else {
                    return (Vec::new(), 0);
                }
        

            return (ret, 1);
        }

        {
            let lowest_depth = self.lowest_depth.lock().unwrap();
            if *lowest_depth != 0 {
                if *lowest_depth <= depth
                //if *end_depth <= depth && end != start
                {
                    return (Vec::new(), 0);
                }
            }
        }

        if let Ok(mut links) = self.client.getValidLinks(start) {
            {
                let visited = self.visited.lock().unwrap();
                links = links
                    .into_iter()
                    .filter(|link| {
                        !link.contains(":") && (link == end || !(*visited).contains_key(link))
                    })
                    .collect::<Vec<String>>();
            }

            for link in &links {
                let mut visited = self.visited.lock().unwrap();

                if !(*visited).contains_key(&link.clone()) {
                    (*visited).insert(link.clone(), depth + 1);
                }
            }

            results = links
                .par_iter()
                .map(|link| {
                    return self.getPath(&link.clone(), end.clone(), depth + 1);
                })
                .collect::<Vec<(Vec<Vec<String>>, u32)>>();

            results = results
                .into_iter()
                .filter(|(_, score)| *score != 0)
                .collect::<Vec<(Vec<Vec<String>>, u32)>>();
            results.sort_by(|(_, score1), (_, score2)| score1.partial_cmp(score2).unwrap());

            if results.len() > 0 {
                let (res, lowest_score) = results[0].clone();

                let lowest_depth = self.lowest_depth.lock().unwrap();
                if *lowest_depth != 0 && *lowest_depth < lowest_score + depth
                //if *end_depth <= depth && end != start
                {
                    return (Vec::new(), 0);
                }

                let (ret, score) = results.into_iter().fold(
                    (Vec::new(), 0),
                    |acc: (Vec<Vec<String>>, u32), (res_vec, score)| {
                        let (mut paths, path_score) = acc;
                        if score == lowest_score {
                            println!(
                                "added paths {:?}, (path depth {}, my depth {})",
                                res_vec, score, depth
                            );
                            for mut added_path in res_vec {
                                added_path.push(String::from(start));
                                paths.push(added_path);
                            }
                        }
                        (paths, lowest_score)
                    },
                );

                return (ret, score + 1);
            }
        }

        return (Vec::new(), 0);
    }
}
