use std::env;

use rayon::ThreadPool;

#[macro_use]
mod Wikipedia;
use Wikipedia::WikiSearcher;

const g_nonWords: &[&str] = &["of", "the", "in", "a"];

static mut w: Option<WikiSearcher> = None;
static mut _args: Option<Vec<String>> = None;

fn main() {
    let mut path = vec!["Google", "Israel"];


    unsafe {

        _args = Some(env::args().collect());

        if let Some( args) = &mut _args {
            args.truncate(3);
            let mut iter = args.iter();
            iter.next();

            for (i, arg) in iter.enumerate() {
                path[i] = arg;
            }
        }

        println!("{:?}", path);

        rayon::ThreadPoolBuilder::new().num_threads(70).build().unwrap().install(|| {
            w = Some(WikiSearcher::new());

            if let Some(wiki) = &w {
                println!("res {:#?}", wiki.getPath(path[0], path[1], 0));
            }
            return;
        });
        
    }
}
