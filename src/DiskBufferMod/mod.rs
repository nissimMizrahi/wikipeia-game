extern crate memmap;

use std::{
    fs::OpenOptions,
    io::{Seek, SeekFrom, Write},
};


pub struct DiskBuffer
{
    pub buffer: Option<memmap::MmapMut>,
    file: Option<std::fs::File>,

    path: String,
    length: u64
}

impl DiskBuffer
{
    pub fn new(name: &str, size: u64) -> DiskBuffer
    {
        let path = &format!("{}.buff", name);

        let mut f = OpenOptions::new()
        .read(true)
        .write(true)
        .create(true)
        .open(path)
        .expect("Unable to open file");

        f.set_len(size);

        let mut data = unsafe {
            memmap::MmapOptions::new()
                .map_mut(&f)
                .expect("Could not access data from memory mapped file")
        };

        DiskBuffer{
            buffer: Some(data),
            file: Some(f),

            path: path.clone(),
            length: size
        }

    }
}

impl Drop for DiskBuffer {
    fn drop(&mut self) {

        self.buffer = None;
        self.file = None;


        std::fs::remove_file(&self.path).unwrap();
        return
    }
}